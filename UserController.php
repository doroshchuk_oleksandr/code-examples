<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * @Rest\Route("/")
 */
class ApiController extends FOSRestController
{
    /**
     * Add favorite goods
     *
     * ### Status Code 200 ###
     *
     *     {
     *       "success": {
     *           "code": "integer",
     *           "message": "String",
     *       }
     *     }
     * ### Status Code 401, 403, 404 ###
     *
     *     {
     *       "error": {
     *           "code": "integer",
     *           "message": "String",
     *       }
     *     }
     *
     *
     * @Rest\Post("/users/{user_uniq_id}/favorites")
     * @ApiDoc(
     *  description="Add favorite good",
     *  section="User",
     *  resource=true,
     *  requirements={
     *      {
     *          "name"="_format",
     *          "dataType"="string",
     *          "requirement"="json",
     *      },
     *     {
     *          "name"="user_uniq_id",
     *          "dataType"="string"
     *      }
     *  },
     *  parameters={
     *      {"name"="goods_uniq_id", "dataType"="string", "required"=true, "description"="Good ID"},
     *  },
     *  statusCodes={
     *      200={
     *          "Added favorite good for user",
     *          "This goods are already favorite",
     *      },
     *      401="User not authorized",
     *      403="Forbidden",
     *      404="Good not found"
     *  },
     *     tags={
     *         "ready for testing" = "#c0cc0c"
     *     }
     * )
     *
     */
    public function addFavoriteGoodAction(Request $request)
    {
        $data = $request->request;
        $userUUID = $data->get('user_uniq_id');

        $checkUserService = $this->get('yami.check.user.service');

        if (!is_object(($user = $checkUserService->checkUser($userUUID)))) {
            $responseArray = json_decode($user, true);

            return new View($responseArray, $responseArray['error']['code']);
        }

        $goodsId = $data->get('goods_uniq_id');
        $em = $this->get('doctrine.orm.entity_manager');
        $checkGoodsFavorite = $em->getRepository('ApiBundle:User')->checkFavoritesGoods($goodsId, $userUUID);

        if (!empty($checkGoodsFavorite)) {
            $response['success'] = [
                'code' => Response::HTTP_OK,
                'message' => 'This goods are already favorite'
            ];

            return new View($response, Response::HTTP_OK);
        }

        $goods = $em->getRepository('ApiBundle:Goods')->findOneBy(['uuid' => $goodsId]);

        if (!is_object($goods)) {
            $response['error'] = [
                'code' => Response::HTTP_NOT_FOUND,
                'message' => 'Goods not found',
            ];

            return new View($response, Response::HTTP_NOT_FOUND);
        }

        $user->addGoodsFavorite($goods);

        $em->persist($user);
        $em->flush();

        $response['success'] = [
            'code' => Response::HTTP_OK,
            'message' => 'Added favorite good for user'
        ];

        return new View($response, Response::HTTP_OK);
    }

    /**
     * Get petition image by petition id.
     *
     * ### Status Code 200 ###
     *
     *     {
     *       "success": {
     *           "code": "integer",
     *           "message": "String",
     *       }
     *     }
     * ### Status Code 401, 403, 404 ###
     *
     *     {
     *       "error": {
     *           "code": "integer",
     *           "message": "String",
     *       }
     *     }
     *
     * @Rest\Get("/image/{id}", name="api_get_image")
     * @ApiDoc(
     *  description="Get petition image by petition id.",
     *  section="Info",
     *  resource=true,
     *  parameters={
     *      {"name"="width", "dataType"="integer", "required"=true, "description"="Image Width"},
     *      {"name"="height", "dataType"="integer", "required"=true, "description"="Emage Height"},
     *  },
     *  statusCodes={
     *         200="Returned when successful(body)",
     *         404={"Returned when petition with this id does not exist",
     *              "Returned when image for this petitions does not exist"}
     *     }
     * )
     */
    public function getImageAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $data = $request->query;
        $width = intval($data->get('width'));
        $height = intval($data->get('height'));

        $petiton = $em->getRepository('AppBundle:Petition')->find(floatval($id));

        try {
            $petiton = $this->isObject($petiton);
            $imagePath = $petiton->getWebPath();

            if (!$width && !$height) {
                $responseImage = 'http://' . $request->headers->get('host') . '/' . $imagePath;

                return new RedirectResponse($responseImage, Response::HTTP_MOVED_PERMANENTLY);
            } else {
                $dataManager = $this->get('liip_imagine.data.manager');
                $filterManager = $this->get('liip_imagine.filter.manager');
                $cacheManager = $this->get('liip_imagine.cache.manager');

                try {
                    $binary = $dataManager->find('api_thumb', $imagePath);

                    $filteredBinary = $filterManager->applyFilter($binary, 'api_thumb', [
                        'filters' => [
                            'thumbnail' => [
                                'size' => [$width, $height],
                                'mode' => 'outbound',
                            ]
                        ]
                    ]);

                    $cacheManager->store($filteredBinary, $imagePath, 'api_thumb');

                    $responseImage = $cacheManager->resolve($imagePath, 'api_thumb');

                    return new RedirectResponse($responseImage, Response::HTTP_MOVED_PERMANENTLY);
                } catch (\Exception $e) {
                    $response['error'] = [
                        'code' => Response::HTTP_NOT_FOUND,
                        'message' => 'Image not found',
                    ];

                    return new View(json_encode($response), 404);
                }
            }
        } catch (\Exception $e) {
            $response['error'] = [
                'code' => Response::HTTP_NOT_FOUND,
                'message' => 'Petition not found',
            ];

            return new View(json_encode($response), 404);
        }

    }
}