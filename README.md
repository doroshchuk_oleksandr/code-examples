# README #

В это репозетории есть пару файлов с моим кодом

### Файл DataParserService ###

Этот код используется для множества парсеров, он сабмитит данные с помощью формы, которые мы подогнали под структуру 
данных которые приходят с парсера. Получается достаточно мало кода, при том что сетится могут и до 40 полей в одну таблицу

А так же я сделал сохранение в базу по 100 записей, я протестировал такой вариант и вариант с сохранением всех данных, 
получилось намного быстрее. С одного парсера могло приходить по несколько тысяч записей

### Файл ReportService ###

Строит график по множеству разных условий.
В большинстве вся логика реализована на стороне Mysql

### Файл UserController ###

В этом файле я привел пару примеров API методов, один совсем простенький, другой интереснее.
Второй возвращает картинку нужном размере для приложения используя LiipImagineBundle.