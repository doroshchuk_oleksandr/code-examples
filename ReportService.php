<?php

namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;

class ReportsService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * ReportsService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $id
     * @param $reportSearch
     * @return array
     */
    public function full($id, $reportSearch)
    {
        $dateStart = $reportSearch['start_date'];
        $dateFinish = $reportSearch['finish_date'];

        $types = array_key_exists('source_types', $reportSearch) ? $reportSearch['source_types'] : null;
        $counterIds = array_key_exists('meter_ids', $reportSearch) ? $reportSearch['meter_ids'] : null;
        $label = array_key_exists('labels', $reportSearch) ? $reportSearch['labels'] : null;
        $showWeekends = array_key_exists('show_weekends', $reportSearch) ? $reportSearch['show_weekends'] : null;
        $groupByDay = array_key_exists('sum_by_days', $reportSearch) ? $reportSearch['sum_by_days'] : null;
        $includeZero = array_key_exists('include_zero', $reportSearch) ? $reportSearch['include_zero'] : null;
        $showProcesses = array_key_exists('show_processes', $reportSearch) ? $reportSearch['show_processes'] : null;

        $building = $this->em->getRepository('AppBundle:Building')->findOneBy(['id' => $id, 'isActive' => 1]);
        $sourceTypes = $this->em->getRepository('AppBundle:Counters')->findSourceTypeByBuildingId($id);

        if (!empty($types)) {
            $sourceTypesData = $this->em->getRepository('AppBundle:SourceType')->findSourceTypeByArray($types);
            $counters = $this->em->getRepository('AppBundle:Counters')->findContersBySourceTypesArray($types, $id);
        } else {
            $sourceTypesData = $sourceTypes;
            $counters = $this->em->getRepository('AppBundle:Counters')->findBy([
                'buildingId' => $id,
                'isActive' => true,
                'isVisible' => true,
            ]);
        }

        $processes = [];
        if ($showProcesses) {
            $processes = $this->em->getRepository('AppBundle:Process')->findByDate($id, $dateStart, $dateFinish);
        }

        $counterHistory = [];
        foreach ($sourceTypesData as $key => $sourceType) {
            $data = $this->em->getRepository('AppBundle:CounterHistory')->getCountersStatistics(
                $id, $sourceType['name'],
                $dateStart, $dateFinish,
                $counterIds, $groupByDay,
                $includeZero
            );

            if (count($data) > 0) {
                $counterHistory[$sourceType['name']]['data'] = $data;
                $counterHistory[$sourceType['name']]['color'] = $sourceType['color'];
                $counterHistory[$sourceType['name']]['id'] = $sourceType['id'];
                $counterHistory[$sourceType['name']]['key'] = $key;
            }
        }

        $weekends = [];
        if ($showWeekends) {
            $dateDiff = date_diff(new \DateTime($dateFinish), new \DateTime($dateStart))->days;

            for ($i = 1; $i <= $dateDiff; $i++) {
                $weekend = date("w", strtotime(date('Y-m-d', strtotime($dateStart) + $i * 86400)));
                if ($weekend === 0) {
                    $weekends[] = strtotime(date('Y-m-d 12:00', strtotime($dateStart) + $i * 86400));
                    $i += 5;
                } elseif ($weekend === 6) {
                    $weekends[] = strtotime(date('Y-m-d 12:00', strtotime($dateStart) + $i * 86400));
                }
            }
        }

        return [
            'building' => $building,
            'sourceTypes' => $sourceTypes,
            'sourceTypesData' => $sourceTypesData,
            'counterHistories' => $counterHistory,
            'counters' => $counters,
            'reportSearch' => $reportSearch,
            'label' => $label,
            'weekends' => $weekends,
            'showWeekends' => $showWeekends,
            'groupByDay' => $groupByDay,
            'includeZero' => $includeZero,
            'processes' => $processes,
            'showProcesses' => $showProcesses,
        ];
    }
}
