<?php

namespace AdminNBABundle\Services;

use AdminNBABundle\Services\CurlHelperService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormFactoryInterface;

class NbaDataParserService
{
    const BACH_SIZE = 100;

    /**
     * @var \AdminNBABundle\Services\CurlHelperService
     */
    private $curlService;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var DataMappingService
     */
    private $dataMappingService;

    /**
     * NbaDataParserService constructor.
     * @param \AdminNBABundle\Services\CurlHelperService $curlService
     * @param FormFactoryInterface $formFactory
     * @param EntityManager $em
     * @param DataMappingService $dataMappingService
     */
    public function __construct(CurlHelperService $curlService, FormFactoryInterface $formFactory, EntityManager $em, DataMappingService $dataMappingService)
    {
        $this->curlService = $curlService;
        $this->em = $em;
        $this->formFactory = $formFactory;
        $this->dataMappingService = $dataMappingService;
    }

    /**
     * @return bool
     */
    public function saveAllTeamsDataFromParser()
    {
        $teams = $this->prepareTeamsDataFromParser();

        return $this->makeRequest($teams, 'NbaTeam', 'TeamID', 'adminnbabundle_nbateam');
    }

    /**
     * @return bool
     */
    public function saveAllPlayersDataFromParser()
    {
        $players = $this->preparePlayersDataFromParser();

        return $this->makeRequest($players, 'NbaPlayer', 'PlayerID', 'adminnbabundle_nbaplayer');
    }

    /**
     * @param $results
     * @param $entityType
     * @param $uniqueFieldName
     * @param $formMachineName
     * @return array|bool|string
     */
    private function makeRequest($results, $entityType, $uniqueFieldName, $formMachineName)
    {
        $invalidResult = $this->isResultInvalid($results);

        if ($invalidResult) {
            return $invalidResult;
        }

        foreach ($results as $key => $result) {

            $entity = $this->em->getRepository('AdminNBABundle:' . $entityType)->findOneBy([$uniqueFieldName => $result[$uniqueFieldName]]);

            if (!$entity) {
                $entityName = "AdminNBABundle\\Entity\\" . $entityType;
                $entity = new $entityName();
            }

            $request = new Request([], [$formMachineName => $result]);
            $request->setMethod('POST');
            $formName = "AdminNBABundle\\Form\\" . $entityType . "Type";
            $form = $this->formFactory->create(new $formName(), $entity);

            $form->handleRequest($request);
            $this->em->persist($entity);

            if (($key % self::BACH_SIZE) === 0) {
                $this->em->flush();
                $this->em->clear(); // Detaches all objects from Doctrine!
            }
        }

        $this->em->flush();
        $this->em->clear();

        return true;
    }

    /**
     * @param $results
     * @return bool|string|array
     */
    private function isResultInvalid($results)
    {
        if (is_array($results) && array_key_exists('statusCode', $results)) {
            return $results;
        } elseif (!$results) {
            return 'no_data';
        } else {
            return false;
        }
    }

    /**
     * @return bool|array
     */
    private function prepareTeamsDataFromParser()
    {
        $entities = $this->allTeamsParsing();

        if (!$entities) {
            return false;
        }

        return $entities;
    }

    /**
     * @return bool|array
     */
    private function allTeamsParsing()
    {
        return $this->curlService->getParsingData('Fantasy_Data_Parser', 'teams', 'JSON') ?: false;
    }

    /**
     * @return bool|array
     */
    private function preparePlayersDataFromParser()
    {
        $entities = $this->allPlayersParsing();

        if (!$entities) {
            return false;
        }

        return $entities;
    }

    /**
     * @return bool|array
     */
    private function allPlayersParsing()
    {
        return $this->curlService->getParsingData('Fantasy_Data_Parser', 'Players', 'JSON') ?: false;
    }
}

